# Ansible role for [shorewall](http://shorewall.net).

This role is intented for *shorewall* installing and management from sources. During role playing, any system packages of *shorewall* will be uninstalled. 

Version of *shorewall* that will be installed is determined by ansible variable `shorewall_version`.

Variable `shorewall_enabled` determines state of *shorewall*. *Shorewall* will always start with operating system as a service, if `shorewall_enabled` was defined as truly value. On the contrary, if `shorewall_enabled` was defined as falsy, *shorewall* will never start at system start up and will be stopped immediately.


## Role variables.
|Variable name         | Online doc link                                         |
|----------------------|---------------------------------------------------------|
| shorewall_zones      | http://shorewall.net/manpages/shorewall-zones.html  |
| shorewall_interfaces | http://shorewall.net/manpages/shorewall-interfaces.html |
| shorewall_policy     | http://shorewall.net/manpages/shorewall-policy.html     |
| shorewall_hosts      | http://shorewall.net/manpages/shorewall-hosts.html      |
| shorewall_tunnels    | http://shorewall.net/manpages/shorewall-tunnels.html    |
| shorewall_nat        | http://shorewall.net/manpages/shorewall-nat.html        |
| shorewall_masq       | http://shorewall.net/manpages/shorewall-masq.html       |
| shorewall_rules      | http://shorewall.net/manpages/shorewall-rules.html      |


***NOTE:** Some options that documentation describes, may not be handled by 
this role. For more details, see `default/main.yml`.*
